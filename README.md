# Header 1
## Header 2
### Header 3
#### Header 4
##### Header 5
###### Header 6

**bold** __text__
*italic* _text_
**combined _text_**
~~scratched text~~
`<?php echo "ola mundo" ?>`
> blockquote

- [x] first item
- [X] second item
- [ ] third item

* first item
- second item
+ third item

1. first item
2. second item
3. third item

@guerradake

___
---
***

| tables        | are           | cool  |
| ------------- |:-------------:| -----:|
| col 2 is      | centered      |   $50 |
| col 3 is      | right-aligned |  $100 |
| zebra stripes | are neat      |   $25 |

[CFEP](https://www.cfep.org.br/ "CFEP's Website")